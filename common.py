import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
import corner


class Problem:
    def __init__(self, ndim, seed=8128739):
        np.random.seed(seed)
        cov = np.identity(ndim)
        cov = np.random.rand(ndim, ndim) * 2 - 1
        cov = np.dot(cov, cov.T)
        self.ndim = ndim
        self.cov = cov
        self.inv_cov = np.linalg.inv(cov)
        self.means = np.random.normal(scale=3, size=self.ndim)
        self.ncall = 0

    def reset_ncall(self):
        self.ncall = 0

    def lnL(self, theta):
        self.ncall += 1
        _theta = theta - self.means
        return -0.5 * np.dot(_theta, np.dot(self.inv_cov, _theta))

    @staticmethod
    def u_to_x(u):
        return 10.0 * (2.0 * u - 1.0)

    def plot_cov(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots(1, 1)
        sns.heatmap(self.cov, ax=ax, annot=True)
        ax.set(title="Target covariance")
        return ax

    def plot_rvs(self, size=1e5):
        rvs = multivariate_normal.rvs(mean=self.means, cov=self.cov, size=int(size))
        return corner.corner(rvs, truths=self.means, show_titles=True)

